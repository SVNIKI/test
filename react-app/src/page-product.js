"use strict";

let form = document.querySelector('.block-form');

let inputNameContainer = form.querySelector('.input-name');
let inputRatingContainer = form.querySelector('.input-rating');

let inputName = inputNameContainer.querySelector('.shape__name');
let errorNameElement = inputNameContainer.querySelector('.error');

let inputRating = inputRatingContainer.querySelector('.shape__rating');
let errorNameRating = inputRatingContainer.querySelector('.error2');

function handleSubmit(event){
  event.preventDefault();
  
  let name = inputName.value;
    let errorName = '';
    if (name.length === 0){
      errorName = 'Вы забыли указать имя и фамилию';
      inputName.style.borderColor = '#DA4A0C';
    }else  if (name.length < 2) {
      errorName = 'Имя не может быть короче 2-х символов';
      inputName.style.borderColor = '#DA4A0C';
    }else{
      inputName.style.borderColor = '#888888';
    } 

    errorNameElement.innerText = errorName;
    if (errorName){
      errorNameElement.classList.add('visible');
    } else{
      errorNameElement.classList.remove('visible');
    }
    if (errorName) return;

let rating = +inputRating.value;
  let errorRating = ''; 
  if (rating < 1 || rating > 5 ) {
    errorRating = 'Оценка должна быть от 1 до 5'
    inputRating.style.borderColor = '#DA4A0C';
  }else{
    inputRating.style.borderColor = '#888888';
  }  

  errorNameRating.innerText = errorRating;
  errorNameRating.classList.toggle('visible', errorRating);

}

inputName.value = localStorage.getItem('shape__name');
inputName.oninput = () => {
  localStorage.setItem('shape__name', inputName.value)
};

inputRating.value = localStorage.getItem('shape__rating');
inputRating.oninput = () => {
  localStorage.setItem('shape__rating', inputRating.value)
};

area.value = localStorage.getItem('area');
    area.oninput = () => {
      localStorage.setItem('area', area.value)
};

form.addEventListener('submit', handleSubmit);
form.addEventListener('input', handleSubmit);


//корзина

let cartCount = document.querySelector('.item-count2');
let cartSubmit = document.querySelector('.saidbar__btn');

cartSubmit.addEventListener('click', (evt)=>{
  evt.preventDefault();
cartCount.textContent
  if(cartCount.textContent === ''){
    cartCount.textContent = '1';
    cartSubmit.textContent = 'Товар уже в корзине';
    cartCount.style.display = "block";
    cartSubmit.style = 'background: #888888;';
  }else{
    cartCount.textContent = '';
    cartSubmit.textContent = 'Добавить в корзину';
    cartCount.style.display = "none";
    cartSubmit.style = 'background: #RRGGBBAA' ;
  }
}, 
false);

cartCount.textContent = localStorage.getItem('количество');
cartSubmit.textContent = localStorage.getItem('текст');
cartSubmit.style.backgroundColor = localStorage.getItem('фон кнопки');
cartCount.style.display = localStorage.getItem('отображение счётчика');
cartSubmit.onclick = ()=>{
  localStorage.setItem('количество', cartCount.textContent);
  localStorage.setItem('текст', cartSubmit.textContent);
  localStorage.setItem('фон кнопки', cartSubmit.style.backgroundColor);
  localStorage.setItem('отображение счётчика', cartCount.style.display );

}



///////////////////////////////////////////////////////////////



  

//сердечко

let addFav = document.querySelector('.item-count'); //счетчик
let listFav = document.querySelector('.saidbar-fav'); //сердечко

listFav.addEventListener('click', function(ev) {
  if( ev.target.tagName === 'LI') {
   ev.target.classList.toggle('do');
     //при нажатии на сердце появляется текст в иконке  
    if (addFav.textContent === '0'){
    addFav.textContent = '1';
    addFav.style = "display: block"; 
    }
    else{
    addFav.textContent = '0';
    addFav.style = "display: none";
    }
  }
}, false
); 

/*
addFav.textContent = localStorage.getItem('Количество избранных');
listFav.onclick = ()=>{
  localStorage.setItem('Количество избранных', addFav.textContent);
}

let favCount = document.querySelector('.item-count');
let favSubmit = document.querySelector('.saidbar-fav');

favSubmit.addEventListener('click', (evt)=>{
  evt.preventDefault();

  if(favCount.textContent === ''){
    favCount.textContent = '1';
    favCount.style.display = "block"; 
  }else{
    favCount.textContent = '';
    favCount.style.display = "none"
  }
}, 
false);

favCount.textContent = localStorage.getItem('количество');
favCount.style.display = localStorage.getItem('отображение счётчика');
favSubmit.onclick = ()=>{
  localStorage.setItem('количество', favCount.textContent);
  localStorage.setItem('отображение счётчика', favCount.style.display );
}
*/
