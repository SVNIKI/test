"use strict";
// Упражнение 1
/**
 * если тело цикла начнет выполняться - значит в объекте есть свойства
 */
 let obj = {};

 function isEmpty(obj) {
  for (let key in obj) {
    return false; //возвращает false, если свойство существует
  } 
  return true; //возвращает true для пустого объекта
}
console.log(isEmpty(obj)); // true
obj.age=12;
console.log(isEmpty(obj)); // false

// Упражнение 2 в data.js

// Упражнение 3

/**
 * @param {object} исходные данные 
 * @param {number} percent процент на который нужно увеличить сумму
 * @return  {object} newSalaries - новый обьект с пересчитанными под процент суммами 
 * @return  {number} summ - общая сумма
 */

let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000
};

function raiseSalary(percent) {
  let newSalaries = {};
  for (let key in salaries) {
    let raise = (Number(Math.floor(salaries[key]/ 100 * percent)));
    newSalaries[key] = salaries[key] + raise;
  }
  return newSalaries;
}
function calcSumm(obj) {
  let summ = 0;
  for (let key in obj) {
    summ = summ + obj[key];    //summ += obj[key];
  }
  return summ;
}
let newSalaries = raiseSalary(5);
let summ = calcSumm(newSalaries);

console.log(summ);