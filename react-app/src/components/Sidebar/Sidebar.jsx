import "./Sidebar.css";
import CartButton from "../Cart/CartButton";
import FavButton from "../Fav/FavButton";

function Sidebar() {
  return (
    <div className="specifications__saidbar">
      <form>
        <div className="saidbar-pricing card">
          <div className="pricing__favorites">
            <div className="pricing">
              <div className="pricing__old">
                <p className="pricing__oldprice"> 75 990₽</p>
                <div className="pricing__discount">
                  <p className="pricing__discount8">-8%</p>
                </div>
              </div>
              <p className="pricing__new"> 67 990₽</p>
            </div>
            <FavButton />
          </div>
          <div className="saidbar-delivery">
            <p className="delivery">
              Самовывоз в четверг, 1 сентября — <b> бесплатно</b>
            </p>
            <p className="delivery">
              Курьером в четверг, 1 сентября — <b>бесплатно</b>
            </p>
          </div>
          <CartButton />
        </div>{" "}
      </form>
      <div className="saidbar-advertisement">
        <p className="advertisement__name">Реклама</p>
        <div className="advertisement__spisok">
          <iframe className="advertisement__one" src=""></iframe>
          <iframe className="advertisement__two" src=""></iframe>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
