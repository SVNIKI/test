"use strict";

let form = document.querySelector('.block-form');

let inputNameContainer = form.querySelector('.input-name');
let inputRatingContainer = form.querySelector('.input-rating');

let inputName = inputNameContainer.querySelector('.shape__name');
let errorNameElement = inputNameContainer.querySelector('.error');

let inputRating = inputRatingContainer.querySelector('.shape__rating');
let errorNameRating = inputRatingContainer.querySelector('.error');

let inputNam = form.querySelector('.input-name .shape__name');
let inputNa = form.querySelector('.input-name ');


function handleStorage(event){
  inputNam.value = event.newValue;
  }

function handleSubmit(event){
  event.preventDefault();
  
  let nam = inputNam.value;
  let name = inputName.value;
  let rating = +inputRating.value;

  let errorName = '';
    if (name.length === 0){
      errorName = 'Вы забыли указать имя и фамилию';
      inputName.style.borderColor = '#DA4A0C';
    }else  if (name.length < 2) {
      errorName = 'Имя не может быть короче 2-х символов';
      inputName.style.borderColor = '#DA4A0C';
    }else{
      inputName.style.borderColor = '#888888';
    }
  
    errorNameElement.innerText = errorName;
    if (errorName){
      errorNameElement.classList.add('visible');
    } else{
      errorNameElement.classList.remove('visible');
    }

  if (errorName) return;

  let errorRating = ''; 
  if (rating < 1 || rating > 5 ) {
    errorRating = 'Оценка должна быть от 1 до 5'
    inputRating.style.borderColor = '#DA4A0C';
  }else{
    inputRating.style.borderColor = '#888888';
  }

  errorNameRating.innerText = errorRating;
  errorNameRating.classList.toggle('visible', errorRating);


  localStorage.setItem('name', nam);
localStorage.removeItem('shape__name');
  console.log("submit");
}

function handleInput(event) {
  let value = event.target.value;
  let name = event.target.getAttribute('name');
 localStorage.setItem('name', event.target.value);
  localStorage.setItem(name, value);
 }

inputNam.value = localStorage.getItem('shape__name');

window.addEventListener('storage', handleStorage);
form.addEventListener('submit', handleSubmit);
inputNam.addEventListener('input', handleSubmit);
inputNa.addEventListener('input', handleInput);








/*
import './Sidebar.css';
import {useState} from "react"

function Sidebar() {
    const [state, setState] = useState(0);
    return(
        <div className="specifications__saidbar">
                                    <h2 className="item-count2">{ `${state}`}</h2>
            <form >
                <div className="saidbar-pricing card">
                    <div className="pricing__favorites">
                    <div className="pricing">
                        <div className="pricing__old">
                            <p className="pricing__oldprice"> 75 990₽</p>
                            <div className="pricing__discount"><p className="pricing__discount8">-8%</p></div>
                        </div>
                        <p className="pricing__new"> 67 990₽</p>
                    </div>
                    <ul className="saidbar-fav">
                        <li  type="submit" className="favorites ls addFav"></li>
                    </ul>
                    </div>
                        <div className="saidbar-delivery">
                            <p className="delivery">Самовывоз в четверг, 1 сентября — <b> бесплатно</b></p>
                            <p className="delivery">Курьером в четверг, 1 сентября — <b>бесплатно</b></p>
                        </div>
                    <button type="submit" className="saidbar__btn" 
                        onClick={()=>{
                            setState((prev)=>{
                            return prev === 0 ? 1 : 0;
                        });

                    }
                    }>
                        <img src="https://i.ibb.co/920sSSn/Vector-1.png" alt='корзина'></img>
                        {state > 0 ? "Товар уже в корзине" : "Добавить в корзину"}
                        
                     </button>
                </div>    
            </form>
                <div className="saidbar-advertisement">
                        <p className="advertisement__name">Реклама</p>
                    <div className="advertisement__spisok">
                        <iframe className="advertisement__one"src="" > </iframe> 
                        <iframe className="advertisement__two"src=""> </iframe>
                    </div>
                </div>
        </div>

    );
}

export default Sidebar;*/