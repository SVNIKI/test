import "./Breadcrumbs.css";
import { Link } from "react-router-dom";

function Breadcrumbs() {
  return (

    <div className="container__breadcr">
      <nav>
        <Link className="breadcr__links" to="!#">
          Электроника{" "}
        </Link>
        &gt;
        <Link className="breadcr__links" to="!#">
          {" "}
          Смартфоны и гаджеты{" "}
        </Link>
        &gt;
        <Link className="breadcr__links" to="!#">
          {" "}
          Мобильные телефоны{" "}
        </Link>
        &gt;
        <Link className="breadcr__links" to="!#">
          {" "}
          Apple
        </Link>
      </nav>
    </div>
  );
}

export default Breadcrumbs;
