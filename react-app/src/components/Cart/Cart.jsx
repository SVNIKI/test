import { useSelector } from "react-redux";
const iphone = { id: 4884, name: "IPhone 13" };

function Cart() {
  const products = useSelector((state) => state.cart.products);
  const count = products.length;

  const hasInCart = products.some((prevProduct) => {
    return prevProduct.id === iphone.id;
  });

  return (
    <div className="item__none">
      {hasInCart ? (
        <span className="item-count2">{` ${count}`}</span>
      ) : (
        <span className="item__noneee"></span>
      )}
    </div>
  );
}

export default Cart;
