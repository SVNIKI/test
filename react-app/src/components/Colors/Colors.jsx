import React, {useState} from 'react';
import './Colors.css';

const colors = [
{
        id: 'красный',
        image: `https://i.ibb.co/nLQ4c6H/color-1-2.png`,
        alt: 'IPhone 13 цвет красный'
} ,
{
        id: 'зеленый',
        image: 'https://i.ibb.co/68yRQ7r/color-2-1.png',
        alt: 'IPhone 13 цвет зеленый'
} ,
{
        id:'розовый',
        image: 'https://i.ibb.co/smjT3dd/color-3-1.png',
        alt: 'IPhone 13 цвет розовый'
} ,
{
        id: 'синий',
        image: 'https://i.ibb.co/hCGdFvw/color-4-1.png',
        alt: 'IPhone 13 цвет синий'
} ,
{
        id: 'белый',
        image: 'https://i.ibb.co/h8KznCB/color-5-1.png',
        alt: 'IPhone 13 цвет белый'
} ,
{
        id: 'черный',
        image: 'https://i.ibb.co/G5VH81d/color-6-1.png',
        alt: 'IPhone 13 цвет черный'
} 
]


function Colors() {
const [colorId, setColorId] = useState(colors[3].id);
const [activedColor, setActivedColor] = useState(3);
    return(
        <div className="characteric">
                <h3 className="characteric__name">Цвет товара: {colorId} </h3>
                <div className="characteric__list">
                        <ul className="list">
                                {colors.map((color, index) =>{
                                
                                const actived = index === activedColor;
                                const activeClass = actived ? "square-image__selected-blue": '';

                                return(
                                <li className={`square-image__selected square-image ${activeClass } `}
                                key ={color.id}
                                onClick={() =>{
                                        setActivedColor(index)
                                        setColorId(colors[index].id)
                                    }}
                                >
                                <img  className="square-image__image" alt={color.alt} src={color.image}/>
                                </li>
                                );
                                })}
                        </ul>
                </div>
        </div>
    );
}

export default Colors;


