import "./Сomparison.css";

function Сomparison() {
  return (
    <section className="comparison">
      <h3 className="comparison__name">Сравнение моделей</h3>
      <table className="comparison__table">
        <thead>
          <tr>
            <th>
              {" "}
              <div className="comparison__thead">Модель</div>
            </th>
            <th>
              <p className="comparison__thead">Вес</p>
            </th>
            <th>
              <p className="comparison__thead">Высота</p>
            </th>
            <th>
              <p className="comparison__thead">Ширина</p>
            </th>
            <th>
              <p className="comparison__thead">Толщина</p>
            </th>
            <th>
              <p className="comparison__thead">Чип</p>
            </th>
            <th>
              <p className="comparison__thead">Объём памяти</p>
            </th>
            <th className="comparison__non comparison__bottom">
              <p className="comparison__thead">Аккумулятор</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr className="comparison__text">
            <td>
              <p className="comparison__tbody">Iphone11</p>
            </td>
            <td>
              <p className="comparison__tbody"> 194 грамма</p>
            </td>
            <td>
              <p className="comparison__tbody">150.9 мм</p>{" "}
            </td>
            <td>
              <p className="comparison__tbody">75.7 мм </p>
            </td>
            <td>
              <p className="comparison__tbody">8.3 мм</p>
            </td>
            <td>
              <p className="comparison__tbody">A13 Bionicchip</p>
            </td>
            <td>
              <p className="comparison__tbody">до 128 Гб</p>
            </td>
            <td className="comparison__non">
              {" "}
              <p className="comparison__tbody">До 17 часов </p>
            </td>
          </tr>
          <tr className="comparison__text">
            <td>
              <p className="comparison__tbody">Iphone12</p>
            </td>
            <td>
              <p className="comparison__tbody">164 грамма</p>
            </td>
            <td>
              <p className="comparison__tbody">146.7 мм</p>
            </td>
            <td>
              <p className="comparison__tbody">7.4 мм</p>
            </td>
            <td>
              <p className="comparison__tbody">7.4 мм</p>
            </td>
            <td>
              <p className="comparison__tbody">A14 Bionicchip</p>
            </td>
            <td>
              <p className="comparison__tbody">до 256 Гб</p>
            </td>
            <td className="comparison__non">
              <p className="comparison__tbody">До 19 часов</p>
            </td>
          </tr>
          <tr className="comparison__text">
            <td>
              <p className="comparison__tbody">Iphone13</p>
            </td>
            <td>
              <p className="comparison__tbody">174 грамма</p>
            </td>
            <td>
              <p className="comparison__tbody">146.7 мм</p>
            </td>
            <td>
              <p className="comparison__tbody">71.5 мм</p>
            </td>
            <td>
              <p className="comparison__tbody">7.65 мм</p>
            </td>
            <td>
              <p className="comparison__tbody">A15 Bionicchip</p>
            </td>
            <td>
              <p className="comparison__tbody">до 512 Гб</p>
            </td>
            <td className="comparison__non">
              <p className="comparison__tbody">До 19 часов</p>
            </td>
          </tr>
        </tbody>
      </table>
    </section>
  );
}

export default Сomparison;
