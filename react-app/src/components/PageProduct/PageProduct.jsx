import React from "react";
import "./PageProduct.css";
import Breadcrumbs from "../Breadcrumbs/Breadcrumbs.jsx";
import Colors from "../Colors/Colors.jsx";
import Configs from "../Configs/Configs.jsx";
import Сomparison from "../Сomparison/Сomparison.jsx";
import Review from "../Review/Review.jsx";
import Feedback from "../Feedback/Feedbackk.jsx";
import Sidebar from "../Sidebar/Sidebar.jsx";
import { Link } from "react-router-dom";

function PageProduct() {
  return (
    <>
      <div className="container">
        <Breadcrumbs />
        <h2 className="main-title">Смартфон Apple iPhone 13, cиний</h2>

        <div className="container__product-photos">
          <div className="product-photos">
            <img
              className="product-photos__photo"
              alt="IPhone 13"
              src="https://i.ibb.co/gD0NxRc/image-1.webp"
            ></img>
            <img
              className="product-photos__photo"
              alt="IPhone 13"
              src="https://i.ibb.co/hCLxFFc/image-2.webp"
            ></img>
            <img
              className="product-photos__photo"
              alt="IPhone 13"
              src="https://i.ibb.co/K2XzgpF/image-3.webp"
            ></img>
            <img
              className="product-photos__photo"
              alt="IPhone 13"
              src="https://i.ibb.co/kX7JcCT/image-4.webp"
            ></img>
            <img
              className="product-photos__photo"
              alt="IPhone 13"
              src="https://i.ibb.co/gyXxZBQ/image-5.webp"
            ></img>
          </div>
        </div>

        <div className="container__specifications">
          <div className="specifications__all">
            <Colors />
            <Configs />
            <div className="haraktert">
              <h3 className="haraktert__name">Характеристики товара</h3>
              <div className="haraktert__spisok">
                <ul className="haraktert__ul">
                  <li className="haraktert__li">
                    <span className="haraktert__color">
                      Экран: <b> 6.1</b>
                    </span>
                  </li>
                  <li className="haraktert__li">
                    <span className="haraktert__color">
                      Встроенная память: <b> 128 ГБ</b>
                    </span>
                  </li>
                  <li className="haraktert__li">
                    <span className="haraktert__color">
                      Операционная система: <b> iOS 15</b>
                    </span>
                  </li>
                  <li className="haraktert__li">
                    <span className="haraktert__color">
                      Беспроводные интерфейсы: <b> NFC, Bluetooth, Wi-Fi </b>{" "}
                    </span>
                  </li>
                  <li className="haraktert__li">
                    <span className="haraktert__color">
                      Процессор:{" "}
                      <b>
                        {" "}
                        <Link
                          to="https://ru.wikipedia.org/wiki/Apple_A15"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="haraktert__link"
                        >
                          Apple A15 Bionic
                        </Link>{" "}
                      </b>
                    </span>
                  </li>
                  <li className="haraktert__li">
                    <span className="haraktert__color">
                      Вес: <b>173 г</b>
                    </span>
                  </li>
                </ul>
              </div>
            </div>

            <div className="description">
              <h3 className="description__name">Описание</h3>
              <p className="description__paragraf">
                Наша самая совершенная система двух камер. <br></br> Особый
                взгляд на прочность дисплея. <br></br>Чип, с которым всё
                супербыстро.<br></br> Аккумулятор держится заметно дольше.
                <br></br> <i> iPhone 13 - сильный мира всего.</i>
              </p>
              <p className="description__paragraf">
                Мы разработали совершенно новую схему расположения и
                развернулиобъективы на 45 градусов. Благодаря этому внутри
                корпуса поместилась нашалучшая система двух камер с увеличенной
                матрицей широкоугольной камеры.Кроме того, мы освободили место
                для системы оптической стабилизацииизображения сдвигом матрицы.
                И повысили скорость работы матрицы насверхширокоугольной камере.
              </p>
              <p className="description__paragraf">
                Новая сверхширокоугольная камера видит больше деталей в тёмных
                областяхснимков. Новая широкоугольная камера улавливает на 47%
                больше света для более качественных фотографий и видео. Новая
                оптическая стабилизация сосдвигом матрицы обеспечит чёткие кадры
                даже в неустойчивом положении.
              </p>
              <p className="description__paragraf">
                Режим «Киноэффект» автоматически добавляет великолепные эффекты
                перемещенияфокуса и изменения резкости. Просто начните запись
                видео. Режим «Киноэффект»будет удерживать фокус на объекте
                съёмки, создавая красивый эффект размытиявокруг него. Режим
                «Киноэффект» распознаёт, когда нужно перевести фокус на
                другогочеловека или объект, который появился в кадре. Теперь
                ваши видео будут смотретьсякак настоящее кино.
              </p>
            </div>
            <Сomparison />
            <Review />
            <Feedback />
          </div>
          <Sidebar />
        </div>
      </div>
    </>
  );
}
export default PageProduct;
