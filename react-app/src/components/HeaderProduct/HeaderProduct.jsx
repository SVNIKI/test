import React from "react";
import "./HeaderProduct.css";
import { Link } from "react-router-dom";
import Cart from "../Cart/Cart";
import Fav from "../Fav/Fav";

function HeaderProduct() {
  return (
    <header className="head">
      <div className="header">
        <Link className="header__decor" to="/">
          <h1 className="header__name">
            {" "}
            <img
              className="header__logo"
              src="https://i.ibb.co/chN3gv6/image.png"
              alt="Логотип иконка"
            ></img>{" "}
            <span className="header__name-style">Мой</span>Маркет
          </h1>{" "}
        </Link>
        <div className="header__icons">
          <form>
            <div className="wrapper">
              <div className="cart-nav">
                <div className="icon">
                  <span className="icons__heart"></span>
                </div>
                <Fav />

                <div className="icon">
                  <span className="icons__basket"></span>
                </div>
                <Cart />
              </div>
            </div>
          </form>
        </div>
      </div>
    </header>
  );
}

export default HeaderProduct;
