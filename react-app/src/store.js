import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./components/Reducer/cart-reducer";
import favReducer from "./components/Reducer/fav-reducer";

export const store = configureStore({
  reducer: {
    cart: cartReducer,
    fav: favReducer
  }
});
