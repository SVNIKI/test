import React from "react";
import "./FooterProduct.css";
import { Link } from "react-router-dom";
import { useCurrentDate } from "@kundinos/react-hooks";

function FooterProduct() {
  const currentDate = useCurrentDate();
  const fullYear = currentDate.getFullYear();

  return (
    <footer>
      <div className="foote">
        <div className="cellars">
          <div className="cellar">
            <div className="cellar__content">
              <ul className="cellar__list">
                <li className="cellar__text">
                  {" "}
                  <b>
                    {" "}
                    © ООО «<span className="header__name-style">Мой</span>
                    Маркет», 2018-{`${fullYear}`}{" "}
                  </b>
                </li>
                <li className="cellar__text">
                  {" "}
                  Для уточнения информации звоните по номеру{" "}
                  <a className="cellar__link" href="tel:79000000000">
                    +7 900 000 0000
                  </a>
                  , <br className="cellar__text_br"></br>а предложения по
                  сотрудничеству отправляйте на почту{" "}
                  <a
                    className="cellar__link"
                    href="mailto:partner@mymarket.com"
                  >
                    partner@mymarket.com
                  </a>
                </li>
              </ul>
            </div>
            <Link
              to="/product"
              title="Вернуться к началу страницы"
              className="cellar__up"
            >
              Наверх
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default FooterProduct;
