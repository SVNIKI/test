import React, {useState} from 'react';
import './Configs.css';

const configs = [
    {
        id: 128,
        gb: '128 ГБ'
    } ,
    {
        id: 256,
        gb: '256 ГБ'
    } ,
    {
        id: 512,
        gb: '512 ГБ'
    } 
]

function Configs() {
const [configId, setConfigId] = useState(configs[0].id);
const [activedConfig, setActivedConfig] = useState(0);
    return(
    <div className="configuration">
        <h3 className="configuration__name">Конфигурация памяти: {configId} ГБ</h3>
        <ul className="configuration__form">
        {configs.map((config, index) =>{

        const actived = index === activedConfig;
        const activeClass = actived ? "btn_selected128": '';
        
        return(
<li key={index}>
<button className={`configuration__button configuration_dang btn_selected  ${activeClass } `} 
onClick={() =>{
    setActivedConfig(index)
    setConfigId(configs[index].id)
}} > 
<p className="configuration__gb">{config.gb}</p></button>
</li>
        );
    })}   
        </ul>
    </div>

    );
}

export default Configs;