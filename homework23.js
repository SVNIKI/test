"use strict";
// Упражнение 1
let n = Number(+prompt("Введите число",""));
let intervalId = setInterval(()=>{
    if (isNaN(n)){
        console.log("Ошибка, вы ввели не число");
        clearInterval(intervalId);
    } else {
        n = n - 1;
        console.log("Осталось " +n);
        if (n===0) {
            console.log("Время вышло!");
            clearInterval(intervalId);
        }
    }
}, 1000);


// Упражнение 2
let promise = fetch("https://reqres.in/api/users");

promise

.then(function(response) {
    return response.json();
})

.then(function(response) {
    let users = response.data;
    let message = '';

    message += `Получили пользователей: ${users.length} \n`;

    users.forEach(function(users) {
    message +=  `— ${users.first_name} ${users.last_name} (${users.email})\n`;
});
    console.log(message);
})

.catch((error) => {
    console.error(error);
  })