import { useSelector, useDispatch } from "react-redux";
import { addProduct, removeProduct } from "../Reducer/cart-reducer";

const iphone = { id: 4884, name: "IPhone 13" };

function CartButton() {
  const products = useSelector((state) => state.cart.products);
  const dispatch = useDispatch();

  const hasInCart = products.some((prevProduct) => {
    return prevProduct.id === iphone.id;
  });

  function handleRemoveProduct(e, product) {
    e.preventDefault();
    dispatch(removeProduct(product));
    localStorage.removeItem("cart");

  }

  function handleAddProduct(e, product) {
    e.preventDefault();
    dispatch(addProduct(product));
    localStorage.setItem("cart", JSON.stringify(product));

  }

  return (
    <div>
      {hasInCart ? (
        <button
          onClick={(e) => handleRemoveProduct(e, iphone)}
          type="submit"
          className="saidbar__btn__tap"
        >
          <img src="https://i.ibb.co/920sSSn/Vector-1.png" alt="корзина"></img>
          Товар уже в корзине
        </button>
      ) : (
        <button
          onClick={(e) => handleAddProduct(e, iphone)}
          type="submit"
          className="saidbar__btn"
        >
          <img src="https://i.ibb.co/920sSSn/Vector-1.png" alt="корзина"></img>
          Добавить в корзину
        </button>
      )}
    </div>
  );
}
export default CartButton;
