import './Feedback.css';
import React, { useState } from "react";

function Feedback ()  {
const [name, setName] = useState(localStorage.getItem('name') ||"");
const [rating, setRating] = useState(localStorage.getItem('rating') || "");
const [nameError, setNameError] = useState("");
const [ratingError, setRatingError] = useState("");

    function handleSubmit  (event) {
        event.preventDefault();
        validateName();
        validateRating();
        clearValidate();
    }

    let inputName = false;
    function validateName()  {
        if (name.trim() === '' || name.length === 0  ) {
            setNameError('Вы забыли указать имя и фамилию')
        }
        else if (name.length <= 2) {
            setNameError('Имя не может быть короче 2х-символов')
        } else {
            inputName = true;
        }
    }

    let inputRating = false;
    function validateRating()  {
        if (rating >= 1 && rating <= 5) {
            inputRating = true;
        } else {
            setRatingError('Оценка должна быть от 1 до 5');
        }
    }

    function clearValidate() {
        if (inputName === false && inputRating === false) {
            setRatingError('')
        }
        if (inputName === true && inputRating === true) {
        setName('')
        setRating('')
        localStorage.removeItem('name')
        localStorage.removeItem('rating')
        }
    }

    const handleInputName= (e)=>{
        setName(e.target.value); 
        setNameError('');
        localStorage.setItem('name', e.target.value)
    };

    const handleInputRating= (e)=>{
        setRating(e.target.value); 
        setRatingError('');
        localStorage.setItem('rating', e.target.value)
    };
  
return(
    <form className="block-form" onSubmit={handleSubmit}>
        <h3 className="block-form__name"> Добавить свой отзыв</h3>
            <div className="block-form__shape">
                <div className="shape__form">
                    
                    <div className="input-name">
                        <input  className="shape__name" 
                        value={name}
                        onChange={handleInputName}
                        name="name" 
                        type="text"  
                        placeholder="Имя и фамилия"></input>
                        <div className={nameError ? 'error' : ''}>
                            {nameError}
                        </div>
                    </div>

                    <div className="input-rating">
                    <input className="shape__rating"                             
                    value={rating}
                    onChange={handleInputRating}
                    type="number" 
                    name="rating"  
                    placeholder="Оценка"></input>  
                        <div className={ratingError?"error2": ''}>
                            {ratingError}
                        </div>
                    </div>

                </div>
                    <textarea id="area"className="shape__text" name="text"  type="text"rows="9" cols="100" placeholder="Текст отзыва"></textarea>
            </div>
            <button type="submit" className="block-form__button">Отправить отзыв</button>
    </form>
    )
}
export default Feedback;