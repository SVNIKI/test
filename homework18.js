"use strict";
// Упражнение 1
let a = '100px';
let b = '323px';

let result=parseInt(a)+parseInt(b);
console.log(result); // Выводим в консоль, должнополучится 423


// Упражнение 2
console.log(Math.max( 10, -45, 102, 36, 12, 0, -1) )


// Упражнение 3
let c = 123.3399; // Округлить до 123
// Решение 
console.log(Math.round(c));

let d = 0.111; // Округлить до 1
console.log(Math.ceil(d));
let e = 45.333333; // Округлить до 45.3
console.log( Number(e.toFixed(1)));
let f = 3; // Возвести в степень 5 (должно быть 243)
console.log(f**5);
let g = 400000000000000; // Записать в сокращенном виде
console.log( 4e14 );
let h ='1'!== 1; // Поправить условие, чтобы результат был true (значения изменять нельзя, только оператор)
console.log(h!==1);


// Упражнение 4
console.log( 0.1 + 0.2 === 0.3);// Вернёт false, почему?
//потеря точности сумма 0.1 и 0.2 не равна 0.3  
console.log( 0.1 + 0.2 ); // 0.30000000000000004
// Правила округления обычно не позволяют нам увидеть эту «крошечную потерю точности», но она существует.
//И когда мы суммируем 2 числа, их «неточности» тоже суммируются. Вот почему 0.1 + 0.2 – это не совсем 0.3.
let sum = 0.1 + 0.2;
console.log( sum.toFixed(2) ); // 0.30