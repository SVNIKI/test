import "./PageIndex.css";
import { Link } from "react-router-dom";

import React from "react";
import FooterProduct from "../FooterProduct/FooterProduct";
import HeaderProduct from "../HeaderProduct/HeaderProduct";


function PageIndex() {
  return (
    <>
      <HeaderProduct />
      <div className="page">
        <div className="page-layout ">
          <div className="page__text">
            Здесь должно быть содержимое главной страницы. Но в рамках курса
            главная страница используется лишь в демонстрационных целях
            <Link className="page__decor" to="/product">
              <p className="page__text-link">Перейти на страницу товара</p>
            </Link>
          </div>
        </div>
      </div>
      <FooterProduct />
    </>
  );
}
export default PageIndex;
