import React from "react";
import FooterProduct from "../FooterProduct/FooterProduct";
import PageProduct from "../PageProduct/PageProduct";
import HeaderProduct from "../HeaderProduct/HeaderProduct";

function Page(props) {
  return (
    <div>
      <HeaderProduct />
      <PageProduct />
      <FooterProduct />
    </div>
  );
}

export default Page;
