import { useSelector, useDispatch } from "react-redux";
import { addFav, removeFav } from "../Reducer/fav-reducer";

const favorit = { id: 4, name: "fav" };

function FavButton() {
  const favs = useSelector((state) => state.fav.favs);
  const dispatch = useDispatch();

  const hasInFav = favs.some((prevFav) => {
    return prevFav.id === favorit.id;
  });

  function handleRemoveFav(e, fav) {
    e.preventDefault();
    dispatch(removeFav(fav));
    localStorage.removeItem("fav");
  }
  
  function handleAddFav(e, fav) {
    e.preventDefault();
    dispatch(addFav(fav));
    localStorage.setItem("fav", JSON.stringify(fav));
  }

  return (
    <div>
      {hasInFav ? (
        <div className="saidbar-fav">
          <button
            onClick={(e) => handleRemoveFav(e, favorit)}
            type="submit"
            className="favorites ls addFav__tab"
          ></button>
        </div>
      ) : (
        <div className="saidbar-fav">
          <button
            onClick={(e) => handleAddFav(e, favorit)}
            type="submit"
            className="favorites ls addFav"
          ></button>
        </div>
      )}
    </div>
  );
}
export default FavButton;
