import { createSlice } from "@reduxjs/toolkit";

export const heartSlice = createSlice({
  name: "heart",

  initialState: {
    favs: []
  },
//JSON.parse(localStorage.getItem) ||
  reducers: {
    addFav: (prevState, action) => {
      //    console.log("action", action);
      const fav = action.payload;
      const hasInFav = prevState.favs.some(
        (prevFav) => prevFav.id === fav.id);

      if (hasInFav) return prevState;

      return {
        ...prevState,
        favs: [...prevState.favs, fav]
      };
    },
    removeFav: (prevState, action) => {
      const fav = action.payload;
      return {
        ...prevState,
        favs: prevState.favs.filter((prevFav) => {
          return prevFav.id !== fav.id;
        })
      };
    }
  }
});

export const { addFav, removeFav } = heartSlice.actions;

export default heartSlice.reducer;
