import { useSelector } from "react-redux";
const favorit = { id: 4, name: "fav" };

function Fav() {
  const favs = useSelector((state) => state.fav.favs);
  const count = favs.length;

  const hasInCart = favs.some((prevProduct) => {
    return prevProduct.id === favorit.id;
  });

  return (
    <div className="item__none">
      {hasInCart ? (
        <span className="item-count">{` ${count}`}</span>
      ) : (
        <span className="item__noneee"></span>
      )}
    </div>
  );
}

export default Fav;
