import React from "react";
import "./PageNotFound.css";
import { Link } from "react-router-dom";
import FooterProduct from "../FooterProduct/FooterProduct";
import HeaderProduct from "../HeaderProduct/HeaderProduct";

import styled from "styled-components";

const Layout = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
padding: 0px;
gap: 20px;
height: 60vh;
`;

function PageNotFound() {
  return (    <div className="page-not-found__component">
    <HeaderProduct />
    <div className="page-not-found">
    <Layout>
        <div className="page-not-found__text">
          <h2> Страница не найдена</h2>К сожалению, неправильно набран адрес,
          или такой страницы на сайте больше не существует.
          <Link className="page-not-found__decor" to="/product">
            <p className="page-not-found__text-link">
              Перейти на страницу товара
            </p>
          </Link>
        </div>
        </Layout>
    </div>
    <FooterProduct />
  </div>
  );
}
export default PageNotFound;
