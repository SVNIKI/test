import React from 'react';
import './Review.css';

const reviews = [
{
    id: 1,
    linkPhoto: `https://i.ibb.co/fdHyK9P/review-1.jpg`,
    name: 'Марк Г.',
    rating: "https://i.ibb.co/kSQyMVY/image.png",
    text: {
        usageExperience:'менее месяца',
        dignities:  `
        это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.`,
        disadvantages:`
        к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное`,
    },
},
{
    id: 2,
    linkPhoto: 'https://i.ibb.co/L6BbBgT/review-2.jpg',
    name: 'Валерий Коваленко',
    rating: "https://i.ibb.co/8g1VXNJ/1.png",
    text: {
        usageExperience:'менее месяца',
        dignities: `
        'OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго.`,
        disadvantages:`
        Плохая ремонтопригодность`,
    },
}
]

function Review() {
    return(
<div className="review-section">
<div className="review-section__header">
    <div className="review-section__wrapper">
        <h3 className="review-section__tirle">Отзывы</h3>
        <span className="review-section__count">425</span>
    </div>
</div>
    {reviews.map((reviews) =>(
        <div key={reviews.id} className="review-section__list" >   
              
        <div className="review" >
        <img className="review__photo" src={reviews.linkPhoto} alt='фото'></img>
        <div className="review__content">
                <h4 className="review__name">{reviews.name}</h4>
                <img className="review__rating" src={reviews.rating} alt='рейтинг 5' ></img>
            <div className="review__parameters">
                <div className="review__parameter"> <b>Опыт использования:</b> {reviews.text.usageExperience}</div>
                <div className="review__parameter"> <strong>Достоинства:</strong><br></br> {reviews.text.dignities}</div>
                <div className="review__parameter"> <strong>Недостатки:</strong><br></br>{reviews.text.disadvantages}</div>
            </div>        
        </div>
        </div>    

        </div>
    ))}
            <div className="separator"></div>    
</div>
    );
}

export default Review;