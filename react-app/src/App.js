import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import PageIndex from "./components/PageIndex/PageIndex";
import Page from "./components/Page/Page";
import PageNotFound from "./components/PageNotFound/PageNotFound";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PageIndex />} />
        <Route path="/product" element={<Page />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
}
export default App;